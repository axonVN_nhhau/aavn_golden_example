[Ivy]
[>Created: Thu Jan 04 08:45:23 ICT 2018]
160BCE36603CDBF1 3.18 #module
>Proto >Proto Collection #zClass
st0 start Big #zClass
st0 B #cInfo
st0 #process
st0 @TextInP .resExport .resExport #zField
st0 @TextInP .type .type #zField
st0 @TextInP .processKind .processKind #zField
st0 @AnnotationInP-0n ai ai #zField
st0 @MessageFlowInP-0n messageIn messageIn #zField
st0 @MessageFlowOutP-0n messageOut messageOut #zField
st0 @TextInP .xml .xml #zField
st0 @TextInP .responsibility .responsibility #zField
st0 @StartRequest f0 '' #zField
st0 @EndTask f1 '' #zField
st0 @RichDialog f3 '' #zField
st0 @PushWFArc f4 '' #zField
st0 @RichDialog f2 '' #zField
st0 @PushWFArc f5 '' #zField
st0 @PushWFArc f6 '' #zField
>Proto st0 st0 start #zField
st0 f0 outLink start.ivp #txt
st0 f0 type com.hau.userservice.portal.startData #txt
st0 f0 inParamDecl '<> param;' #txt
st0 f0 actionDecl 'com.hau.userservice.portal.startData out;
' #txt
st0 f0 guid 160BCE367C44AA2D #txt
st0 f0 requestEnabled true #txt
st0 f0 triggerEnabled false #txt
st0 f0 callSignature start() #txt
st0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
st0 f0 @C|.responsibility Everybody #txt
st0 f0 81 49 30 30 -21 17 #rect
st0 f0 @|StartRequestIcon #fIcon
st0 f1 type com.hau.userservice.portal.startData #txt
st0 f1 569 49 30 30 0 15 #rect
st0 f1 @|EndIcon #fIcon
st0 f3 targetWindow NEW:card: #txt
st0 f3 targetDisplay TOP #txt
st0 f3 richDialogId com.hau.userservice.portal.LoginDialog #txt
st0 f3 startMethod start() #txt
st0 f3 type com.hau.userservice.portal.startData #txt
st0 f3 requestActionDecl '<> param;' #txt
st0 f3 responseActionDecl 'com.hau.userservice.portal.startData out;
' #txt
st0 f3 responseMappingAction 'out=in;
' #txt
st0 f3 windowConfiguration '* ' #txt
st0 f3 isAsynch false #txt
st0 f3 isInnerRd false #txt
st0 f3 userContext '* ' #txt
st0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Open Login Dialog</name>
        <nameStyle>17,7
</nameStyle>
    </language>
</elementInfo>
' #txt
st0 f3 160 42 112 44 -51 -8 #rect
st0 f3 @|RichDialogIcon #fIcon
st0 f4 expr out #txt
st0 f4 111 64 160 64 #arcP
st0 f2 targetWindow NEW:card: #txt
st0 f2 targetDisplay TOP #txt
st0 f2 richDialogId com.hau.userservice.portal.UserManagementPage #txt
st0 f2 startMethod start() #txt
st0 f2 type com.hau.userservice.portal.startData #txt
st0 f2 requestActionDecl '<> param;' #txt
st0 f2 responseActionDecl 'com.hau.userservice.portal.startData out;
' #txt
st0 f2 responseMappingAction 'out=in;
' #txt
st0 f2 windowConfiguration '* ' #txt
st0 f2 isAsynch false #txt
st0 f2 isInnerRd false #txt
st0 f2 userContext '* ' #txt
st0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Open User Management Page</name>
        <nameStyle>25,7
</nameStyle>
    </language>
</elementInfo>
' #txt
st0 f2 320 42 176 44 -83 -8 #rect
st0 f2 @|RichDialogIcon #fIcon
st0 f5 expr out #txt
st0 f5 272 64 320 64 #arcP
st0 f6 expr out #txt
st0 f6 496 64 569 64 #arcP
>Proto st0 .type com.hau.userservice.portal.startData #txt
>Proto st0 .processKind NORMAL #txt
>Proto st0 0 0 32 24 18 0 #rect
>Proto st0 @|BIcon #fIcon
st0 f0 mainOut f4 tail #connect
st0 f4 head f3 mainIn #connect
st0 f3 mainOut f5 tail #connect
st0 f5 head f2 mainIn #connect
st0 f2 mainOut f6 tail #connect
st0 f6 head f1 mainIn #connect
