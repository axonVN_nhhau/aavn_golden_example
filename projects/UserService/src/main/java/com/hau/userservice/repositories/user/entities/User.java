package com.hau.userservice.repositories.user.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(of = { "id" }, callSuper = false)
@Entity()
@Table(name = "[user]")
@NamedQueries({ @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
		@NamedQuery(name = "User.findUserByUsername", query = "SELECT u FROM User u WHERE u.username=:username"),
		@NamedQuery(name = "User.findUsersByUsername", query = "SELECT u FROM User u WHERE u.username LIKE CONCAT('%', :searchValue ,'%')") })
public class User {
	@Id
	@GeneratedValue
	@Column(name = "[id]")
	private long id;

	@Column(name = "[username]", nullable = false, unique = true)
	private String username;

	@Column(name = "[password]", nullable = false)
	private String password;

	@Column(name = "[is_active]", nullable = false)
	private boolean isActive;

	@Column(name = "[expired_date]")
	private Date expiredDate;

	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<UserRole> userRoles;

	public User() {
	}

	public User(String username, String password, boolean isActive, Date expiredDate) {
		this.username = username;
		this.password = password;
		this.isActive = isActive;
		this.expiredDate = expiredDate;
	}

}
