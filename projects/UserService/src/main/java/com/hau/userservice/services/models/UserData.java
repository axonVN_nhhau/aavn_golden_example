/*
 * User Management System
 * This is a sample.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: hau.nguyen@axonactive.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.hau.userservice.services.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(of = { "id" }, callSuper = false)
@JsonInclude(Include.NON_EMPTY)
public class UserData {
	@JsonProperty("id")
	private Long id = null;

	@JsonProperty("username")
	private String username = null;

	@JsonProperty("password")
	private String password = null;

	@JsonProperty("roles")
	private List<UserRoleData> roles = null;

	@JsonProperty("isActive")
	private Boolean isActive = true;

	@JsonProperty("expiredDate")
	private Long expiredDate = null;

	public UserData() {
	}

	public UserData(Long id, String username, List<UserRoleData> roles, Boolean isActive, Long expiredDate) {
		super();
		this.id = id;
		this.username = username;
		this.roles = roles;
		this.isActive = isActive;
		this.expiredDate = expiredDate;
	}

	public UserData(String username, String password, List<UserRoleData> roles, Boolean isActive, Long expiredDate) {
		super();
		this.username = username;
		this.password = password;
		this.roles = roles;
		this.isActive = isActive;
		this.expiredDate = expiredDate;
	}

	@Override
	public String toString() {
		return "UserData [id=" + id + ", username=" + username + ", password=" + password + ", roles=" + roles
				+ ", isActive=" + isActive + ", expiredDate=" + expiredDate + "]";
	}	
	
	
	

}
