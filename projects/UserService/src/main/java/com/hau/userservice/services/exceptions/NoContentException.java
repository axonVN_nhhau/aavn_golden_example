package com.hau.userservice.services.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

public class NoContentException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoContentException(String msg) {
		super(3, msg);
	}

	public NoContentException() {
		super(3, "NO CONTENT");
	}

	@Provider
	public static class BadRequestExceptionMapper implements ExceptionMapper<NoContentException> {
		@Override
		public Response toResponse(NoContentException exception) {
			return Response.status(Status.NO_CONTENT).build();
		}
	}

}
