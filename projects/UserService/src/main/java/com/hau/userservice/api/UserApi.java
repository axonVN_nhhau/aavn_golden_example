package com.hau.userservice.api;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.hau.commons.CommonQualifier.SelectedImplementation;
import com.hau.userservice.services.UserService;
import com.hau.userservice.services.exceptions.InvalidInputException;
import com.hau.userservice.services.exceptions.NoContentException;
import com.hau.userservice.services.exceptions.NotFoundException;
import com.hau.userservice.services.models.UserData;

import io.swagger.annotations.ApiParam;

@Path("/user")
@io.swagger.annotations.Api(description = "the user API")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2017-12-22T01:32:03.386Z")
public class UserApi {

	@Inject
	@SelectedImplementation
	private UserService service;

	@POST
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	@io.swagger.annotations.ApiOperation(value = "Add new user.", notes = "", response = void.class, tags = { "user", })
	@io.swagger.annotations.ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 200, message = "Successful.", response = UserData.class),
			@io.swagger.annotations.ApiResponse(code = 405, message = "Invalid input.", response = void.class) })
	public Response add(@ApiParam(value = "User object to add.", required = true) UserData userData)
			throws InvalidInputException {
		return Response.ok().entity(service.add(userData)).build();
	}

	@DELETE
	@Path("/{userId}")
	@io.swagger.annotations.ApiOperation(value = "Delete a user as provided id.", notes = "", response = void.class, tags = {
			"user", })
	@io.swagger.annotations.ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 200, message = "Successful.", response = UserData.class),
			@io.swagger.annotations.ApiResponse(code = 404, message = "User not found.", response = void.class) })
	public Response delete(
			@ApiParam(value = "ID of the user to be deleted.", required = true) @PathParam("userId") Long userId)
			throws NotFoundException {
		return Response.ok().entity(service.delete(userId)).build();
	}

	@GET
	@Path("/findByUsername")
	@Produces({ "application/json" })
	@io.swagger.annotations.ApiOperation(value = "Find users by given username.", notes = "", response = UserData.class, responseContainer = "List", tags = {
			"user", })
	@io.swagger.annotations.ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 200, message = "Successful.", response = UserData.class, responseContainer = "List"),
			@io.swagger.annotations.ApiResponse(code = 204, message = "No user found.", response = UserData.class, responseContainer = "List"),
			@io.swagger.annotations.ApiResponse(code = 405, message = "Invalid input.", response = void.class) })
	public Response findUsers(
			@ApiParam(value = "Username value to find users.", required = true) @QueryParam("username") String username)
			throws NoContentException {
		return Response.ok().entity(service.findByUsername(username)).build();
	}

	@GET
	@Path("/{userId}")
	@Produces({ "application/json" })
	@io.swagger.annotations.ApiOperation(value = "Get user information.", notes = "", response = UserData.class, tags = {
			"user", })
	@io.swagger.annotations.ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 200, message = "Successful.", response = UserData.class),
			@io.swagger.annotations.ApiResponse(code = 404, message = "User not found.", response = UserData.class) })
	public Response get(
			@ApiParam(value = "ID of the user to be get.", required = true) @PathParam("userId") Long userId)
			throws NotFoundException {
		return Response.ok().entity(service.get(userId)).build();
	}

	@GET
	@Path("/list")
	@Produces({ "application/json" })
	@io.swagger.annotations.ApiOperation(value = "List users with paging.", notes = "", response = UserData.class, responseContainer = "List", tags = {
			"user", })
	@io.swagger.annotations.ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 200, message = "Successful.", response = UserData.class, responseContainer = "List"),
			@io.swagger.annotations.ApiResponse(code = 204, message = "No user found.", response = UserData.class, responseContainer = "List"),
			@io.swagger.annotations.ApiResponse(code = 405, message = "Invalid input.", response = void.class) })
	public Response list(
			@ApiParam(value = "The current page. Default 1.", defaultValue = "1") @DefaultValue("1") @QueryParam("page") Integer page,
			@ApiParam(value = "Config the page total items number. Default 10.", defaultValue = "10") @DefaultValue("10") @QueryParam("pageSize") Integer pageSize)
			throws NoContentException, InvalidInputException {
		return Response.ok().entity(service.list(page, pageSize)).build();
	}

	@POST
	@Path("/{userId}")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	@io.swagger.annotations.ApiOperation(value = "Update the user information as provided id.", notes = "", response = void.class, tags = {
			"user", })
	@io.swagger.annotations.ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 200, message = "Successful.", response = void.class),
			@io.swagger.annotations.ApiResponse(code = 404, message = "User not found.", response = void.class),
			@io.swagger.annotations.ApiResponse(code = 405, message = "Invalid input.", response = void.class) })
	public Response update(
			@ApiParam(value = "ID of the user to be udpated.", required = true) @PathParam("userId") Long userId,
			@ApiParam(value = "User object to update.", required = true) UserData userData)
			throws NotFoundException, InvalidInputException {
		return Response.ok().entity(service.update(userId, userData)).build();
	}
}
