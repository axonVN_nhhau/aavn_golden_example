package com.hau.userservice.repositories.user.impl;

import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import com.hau.commons.CommonQualifier.SelectedImplementation;
import com.hau.userservice.repositories.RepositoryQualifier.JpaRepository;
import com.hau.userservice.repositories.user.UserRepository;
import com.hau.userservice.repositories.user.entities.Role;
import com.hau.userservice.repositories.user.entities.User;
import com.hau.userservice.repositories.user.entities.UserRole;
import com.hau.userservice.repositories.user.entities.converters.UserConverter;
import com.hau.userservice.services.models.UserData;
import com.hau.userservice.services.models.UserRoleData;

@Stateless(name = "JpaUserReposiotry")
@JpaRepository
@SelectedImplementation
public class JpaUserReposiotry implements UserRepository {
	private static final String PUNIT = "userunit";

	@PersistenceContext(unitName = PUNIT, type = PersistenceContextType.TRANSACTION)
	private EntityManager em;

	@Override
	public User get(long userId) {
		return em.find(User.class, userId);
	}

	@Override
	public Collection<User> findUsers(String searchValue) {
		return em.createNamedQuery("User.findUsersByUsername", User.class).setParameter("searchValue", searchValue)
				.getResultList();
	}

	static class RoleError {
		boolean raised = false;
	}

	@Override
	public User add(com.hau.userservice.services.models.UserData userApiModel) {
		User entity = UserConverter.fromApiModel(userApiModel);

		updateEntityRole(entity, userApiModel.getRoles());

		em.persist(entity);
		em.flush();
		return entity;
	}

	public Role getRoleEntityFromUesrRoleApiModel(UserRoleData apiUserRole) {
		try {
			return em.createNamedQuery("Role.findRoleByName", Role.class)
					.setParameter("roleName", apiUserRole.getRole()).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public User find(String username) {
		try {
			return em.createNamedQuery("User.findUserByUsername", User.class).setParameter("username", username)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public User delete(long userId) {
		try {
			User user = em.find(User.class, userId);
			if (user == null)
				return null;
			em.remove(user);
			return user;
		} catch (NoResultException e) {
			return null;
		}

	}

	@Override
	public Collection<User> list(int page, int pageSize) {
		return em.createNamedQuery("User.findAll", User.class).setFirstResult(pageSize * (page - 1))
				.setMaxResults(pageSize).getResultList();
	}

	protected void updateEntityRole(User entity, Collection<UserRoleData> inputRoles) {
		entity.setUserRoles(inputRoles.stream().map(apiUserRole -> getRoleEntityFromUesrRoleApiModel(apiUserRole))
				.filter(role -> role != null).map(role -> new UserRole(entity, role, new Date()))
				.collect(Collectors.toList()));
	}

	protected boolean checkIfUpdateRoles(Collection<UserRoleData> inputRoles) {
		return inputRoles != null && inputRoles.size() > 0;
	}

	protected boolean checkIfUpdatePassword(User entity, String password) {
		return password != null && !entity.getPassword().equals(password);
	}

	protected boolean checkIfUpdateExpiredDate(User entity, Long inputExpiredDate) {
		return inputExpiredDate != null && entity.getExpiredDate().getTime() != inputExpiredDate;
	}

	protected boolean checkIfUpdateActiveState(User entity, Boolean activeState) {
		return activeState != null && entity.isActive() != activeState;
	}

	@Override
	public User update(long userId, UserData userApiModel) {
		User entity = em.find(User.class, userId);

		if (entity == null)
			return null;

		if (checkIfUpdateRoles(userApiModel.getRoles())) {
			entity.getUserRoles().forEach(userRole -> em.remove(userRole));
			entity.getUserRoles().removeAll(entity.getUserRoles());
			em.merge(entity);
			em.flush();
			updateEntityRole(entity, userApiModel.getRoles());
		}

		if (checkIfUpdatePassword(entity, userApiModel.getPassword()))
			entity.setPassword(userApiModel.getPassword());

		if (checkIfUpdateExpiredDate(entity, userApiModel.getExpiredDate()))
			entity.setExpiredDate(new Date(userApiModel.getExpiredDate()));

		if (checkIfUpdateActiveState(entity, userApiModel.getIsActive()))
			entity.setActive(userApiModel.getIsActive());

		em.merge(entity);
		return entity;

	}

	@Override
	public Collection<Role> listSystemRoles() {
		return em.createNamedQuery("Role.findAll", Role.class).getResultList();
	}

}
