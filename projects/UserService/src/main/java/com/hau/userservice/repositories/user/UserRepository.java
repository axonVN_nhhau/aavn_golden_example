package com.hau.userservice.repositories.user;

import java.util.Collection;

import com.hau.userservice.repositories.user.entities.Role;
import com.hau.userservice.repositories.user.entities.User;
import com.hau.userservice.services.models.UserData;

public interface UserRepository {

	User get(long userId);

	Collection<User> findUsers(String searchValue);

	User add(UserData userApiModel);

	User update(long userId, UserData userApiModel);

	User find(String username);

	User delete(long userId);

	Collection<User> list(int page, int pageSize);

	Collection<Role> listSystemRoles();

}
