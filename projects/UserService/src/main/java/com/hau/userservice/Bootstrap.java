package com.hau.userservice;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import io.swagger.jaxrs.config.SwaggerContextService;
import io.swagger.models.Contact;
import io.swagger.models.Info;
import io.swagger.models.Swagger;

public class Bootstrap extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9219968653276590765L;

	@Override
	public void init(ServletConfig config) throws ServletException {
		Info info = new Info().title("User Management System").description("This is a sample.").termsOfService("")
				.contact(new Contact().email("hau.nguyen@axonactive.com"));

		//ServletContext context = config.getServletContext();
		Swagger swagger = new Swagger().info(info);

		new SwaggerContextService().withServletConfig(config).updateSwagger(swagger);

	}
}
