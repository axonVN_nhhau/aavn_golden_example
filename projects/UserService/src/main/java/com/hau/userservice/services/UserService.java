package com.hau.userservice.services;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.Collection;

import javax.inject.Qualifier;
import javax.validation.constraints.NotNull;

import com.hau.userservice.services.exceptions.InvalidInputException;
import com.hau.userservice.services.exceptions.NoContentException;
import com.hau.userservice.services.exceptions.NotFoundException;
import com.hau.userservice.services.models.UserData;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2017-12-22T01:32:03.386Z")

public interface UserService {
	UserData add(UserData userData) throws InvalidInputException;

	UserData delete(long userId) throws NotFoundException;

	Collection<UserData> findByUsername(@NotNull String username) throws NoContentException;

	UserData get(long userId) throws NotFoundException;

	Collection<UserData> list(int page, int pageSize) throws NoContentException, InvalidInputException;

	UserData update(long userId, UserData body) throws NotFoundException, InvalidInputException;

	public interface qualifiers {
		@Qualifier
		@Retention(RUNTIME)
		@Target({ FIELD, TYPE, METHOD, PARAMETER })
		public @interface UserApiServiceV1 {
		}
	}
}
