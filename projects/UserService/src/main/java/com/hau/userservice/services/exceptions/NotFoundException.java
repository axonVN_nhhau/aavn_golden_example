package com.hau.userservice.services.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

public class NotFoundException extends ServiceException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotFoundException(String msg) {
		super(2, msg);
	}

	public NotFoundException() {
		super(2, "NOT FOUND.");
	}

	@Provider
	public static class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {
		@Override
		public Response toResponse(NotFoundException exception) {
			return Response.status(Status.NOT_FOUND)
					.entity("ERROR CODE " + exception.getCode() + ": " + exception.getMessage()).type("text/plain")
					.build();
		}
	}
}
