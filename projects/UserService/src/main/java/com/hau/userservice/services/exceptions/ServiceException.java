package com.hau.userservice.services.exceptions;

public class ServiceException extends Exception {

	private static final long serialVersionUID = 1L;
	private int code;

	public ServiceException(int code, String msg) {
		super(msg);
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public ServiceException() {
		super("SERVICE ERROR.");
	}

}
