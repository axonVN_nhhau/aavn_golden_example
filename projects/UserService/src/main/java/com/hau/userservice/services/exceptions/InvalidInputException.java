package com.hau.userservice.services.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

public class InvalidInputException extends ServiceException {
	private static final long serialVersionUID = 1L;

	public InvalidInputException(String msg) {
		super(2, msg);
	}

	public InvalidInputException() {
		super(2, "BAD REQUEST.");
	}

	@Provider
	public static class BadRequestExceptionMapper implements ExceptionMapper<InvalidInputException> {
		@Override
		public Response toResponse(InvalidInputException exception) {
			return Response.status(Status.BAD_REQUEST)
					.entity("ERROR CODE " + exception.getCode() + ": " + exception.getMessage()).type("text/plain")
					.build();
		}
	}
}