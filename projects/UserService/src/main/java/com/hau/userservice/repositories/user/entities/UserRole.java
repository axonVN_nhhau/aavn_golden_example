package com.hau.userservice.repositories.user.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(of = { "user", "role" }, callSuper = false)
@Entity
@Table(name = "[user_role]")
public class UserRole {
	@Id
	@GeneratedValue
	@Column(name = "[id]")
	private long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "[user_id]", nullable = false)
	private User user;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "[role_id]", nullable = false)
	private Role role;

	@Column(name = "[issued_date]", nullable = false)
	private Date issuedDate;

	public UserRole() {
	}

	public UserRole(User user, Role role, Date issuedDate) {
		this.user = user;
		this.role = role;
		this.issuedDate = issuedDate;
	}

}
