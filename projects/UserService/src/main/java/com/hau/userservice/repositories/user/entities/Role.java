package com.hau.userservice.repositories.user.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(of = { "id" }, callSuper = false)
@Entity
@Table(name = "[role]")
@NamedQueries({ @NamedQuery(name = "Role.findRoleByName", query = "SELECT r FROM Role r WHERE r.name=:roleName"),
		@NamedQuery(name = "Role.findAll", query = "SELECT r FROM Role r")

})
public class Role {
	@Id
	@GeneratedValue
	@Column(name = "[id]")
	private int id;

	@Column(name = "[name]", nullable = false)
	private String name;

	@Column(name = "[description]")
	private String description;
	
	public Role() {}

	public Role(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}
	
	
}
