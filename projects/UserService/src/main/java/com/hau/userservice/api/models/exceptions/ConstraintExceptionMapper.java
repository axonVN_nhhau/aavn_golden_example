package com.hau.userservice.api.models.exceptions;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.NotAllowedException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ConstraintExceptionMapper implements ExceptionMapper<RuntimeException> {

	@Override
	public Response toResponse(RuntimeException exception) {

		if (exception instanceof NotAllowedException) {
			return Response.status(Status.METHOD_NOT_ALLOWED)
					.entity("You are not allowed to use this method for this path.").type("text/plain").build();
		}

		if (exception.getCause() instanceof ConstraintViolationException) {
			return Response.status(Status.BAD_REQUEST).entity("Not input required field(s).").type("text/plain")
					.build();
		}

		return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Server error.").type("text/plain").build();
	}

}
