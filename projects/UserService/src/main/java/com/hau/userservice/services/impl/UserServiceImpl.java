package com.hau.userservice.services.impl;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import com.hau.commons.CommonQualifier.SelectedImplementation;
import com.hau.userservice.repositories.user.UserRepository;
import com.hau.userservice.repositories.user.entities.Role;
import com.hau.userservice.repositories.user.entities.User;
import com.hau.userservice.services.UserService;
import com.hau.userservice.services.UserService.qualifiers.UserApiServiceV1;
import com.hau.userservice.services.exceptions.InvalidInputException;
import com.hau.userservice.services.exceptions.NoContentException;
import com.hau.userservice.services.exceptions.NotFoundException;
import com.hau.userservice.services.models.UserData;
import com.hau.userservice.services.models.UserRoleData;
import com.hau.userservice.services.models.converters.UserDataConverter;

@Stateless(name = "UserApiServiceImpl")
@UserApiServiceV1
@SelectedImplementation
@TransactionManagement(TransactionManagementType.BEAN)
public class UserServiceImpl implements UserService {

	@Inject
	@SelectedImplementation
	UserRepository userRepository;

	public boolean isUsernameExist(String username) {
		return userRepository.find(username) != null;
	}

	public boolean isPasswordValid(String password) {
		return password.length() > 3;
	}

	public boolean isUsernameValid(String username) {
		return username != null && username.length() > 2 && username.length() < 21;
	}

	protected void validateListParams(int page, int pageSize) throws InvalidInputException {
		if (pageSize < 5)
			throw new InvalidInputException("Page size cannot be less than 5!");
		if (page < 1)
			throw new InvalidInputException("Page number cannot be less than 1!");
	}

	protected void validateExpiredDate(long expiredDateTime) throws InvalidInputException {
		if (checkTimstampBeforeTomorrow(expiredDateTime)) {
			throw new InvalidInputException("Expired Date must be after the current date!");
		}
	}

	private boolean checkTimstampBeforeTomorrow(long expiredDateTime) {
	    Date expiredDate = new Date(expiredDateTime);
		SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
		Date currentDate = new Date();
		return format.format(currentDate).equals(format.format(expiredDate)) || expiredDate.getTime() < currentDate.getTime();
	}

	protected void validateInputRoles(Collection<UserRoleData> inputRoles) throws InvalidInputException {
		Collection<Role> roles = userRepository.listSystemRoles();
		for (UserRoleData inputRole : inputRoles) {
			boolean found = false;
			for (Role role : roles) {
				if (role.getName().equals(inputRole.getRole()))
					found = true;
			}
			if (!found)
				throw new InvalidInputException("Invalid Role name!");
		}

	}

	protected void validateAddUserInput(UserData userData) throws InvalidInputException {
		if (userData == null)
			throw new InvalidInputException();
		
		if (!isUsernameValid(userData.getUsername()))
			throw new InvalidInputException("Username is invalid. Username length must be from 3 to 20 characters.");

		if (isUsernameExist(userData.getUsername()))
			throw new InvalidInputException("Duplicated username!");

		if (userData.getPassword() == null)
			throw new InvalidInputException("Password cannot be empty when create new user.");

		if (!isPasswordValid(userData.getPassword()))
			throw new InvalidInputException("Password cannot be less than 4 characters.");

		if (userData.getRoles() == null || userData.getRoles().isEmpty()) {
			throw new InvalidInputException("Must set at least one role!");
		}
		validateInputRoles(userData.getRoles());

		if (userData.getExpiredDate() != null)
			validateExpiredDate(userData.getExpiredDate());
	}

	protected void validateUpdateUserInput(long userId, UserData userData)
			throws NotFoundException, InvalidInputException {
		User user = userRepository.get(userId);
		if (user == null)
			throw new NotFoundException("User not found.");

		if (userData.getUsername() != null)
			throw new InvalidInputException("Cannot change username!");

		if (userData.getRoles() != null)
			validateInputRoles(userData.getRoles());

		if (userData.getPassword() != null) {
			if (!isPasswordValid(userData.getPassword()))
				throw new InvalidInputException(
						"Password cannot be less than 4 characters or set it empty to not change password.");
		}
		if (userData.getExpiredDate() != null)
			validateExpiredDate(userData.getExpiredDate());
	}

	@Override
	@Transactional
	public UserData add(UserData userData) throws InvalidInputException {
		validateAddUserInput(userData);
		User entity = userRepository.add(userData);
		return UserDataConverter.fromEntity(entity);

	}

	@Override
	@Transactional
	public UserData delete(long userId) throws NotFoundException {
		User entity = userRepository.delete(userId);
		if (entity == null) {
			throw new NotFoundException("User not found.");
		}
		return UserDataConverter.fromEntity(entity);
	}

	@Override
	public Collection<UserData> findByUsername(@NotNull String username) throws NoContentException {
		Collection<User> resultSet = userRepository.findUsers(username);

		if (resultSet == null || resultSet.size() < 1) {
			throw new NoContentException();
		}

		return resultSet.stream().map(UserDataConverter::fromEntity).collect(Collectors.toList());

	}

	@Override
	public UserData get(long userId) throws NotFoundException {
		User entity = userRepository.get(userId);
		if (entity == null) {
			throw new NotFoundException("User not found.");
		}
		return UserDataConverter.fromEntity(entity);
	}

	@Override
	public Collection<UserData> list(int page, int pageSize) throws NoContentException, InvalidInputException {
		validateListParams(page, pageSize);

		Collection<User> resultSet = userRepository.list(page, pageSize);

		if (resultSet == null || resultSet.size() < 1) {
			throw new NoContentException();
		}

		return resultSet.stream().map(UserDataConverter::fromEntity).collect(Collectors.toList());
	}

	@Override
	@Transactional
	public UserData update(long userId, UserData userData) throws NotFoundException, InvalidInputException {
		validateUpdateUserInput(userId, userData);
		User entity = userRepository.update(userId, userData);
		return UserDataConverter.fromEntity(entity);
	}
}
