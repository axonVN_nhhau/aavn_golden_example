package com.hau.userservice.repositories.user.entities.converters;

import java.util.Date;

import com.hau.userservice.repositories.user.entities.User;
import com.hau.userservice.services.models.UserData;

public class UserConverter {

	public static User fromApiModel(UserData userApiModel) {
		return new User(userApiModel.getUsername(), userApiModel.getPassword(), userApiModel.getIsActive(),
				userApiModel.getExpiredDate() == null ? null : new Date(userApiModel.getExpiredDate()));
	}
}
