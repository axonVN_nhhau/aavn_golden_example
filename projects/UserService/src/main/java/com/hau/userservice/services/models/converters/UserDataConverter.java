package com.hau.userservice.services.models.converters;

import java.util.stream.Collectors;

import com.hau.userservice.repositories.user.entities.User;
import com.hau.userservice.services.models.UserData;
import com.hau.userservice.services.models.UserRoleData;

public class UserDataConverter {

	public static UserData fromEntity(User user) {
		return new UserData(user.getId(), user.getUsername(), user.getUserRoles().stream()
				.map(userRole -> new UserRoleData(userRole.getRole().getName(), userRole.getIssuedDate().getTime()))
				.collect(Collectors.toList()), user.isActive(),
				user.getExpiredDate() != null ? user.getExpiredDate().getTime() : null);
	}

}
