package com.hau.commons;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

public interface CommonQualifier {

	@Qualifier
	@Retention(RUNTIME)
	@Target({ FIELD, TYPE, METHOD, PARAMETER })
	public @interface SelectedImplementation {
	}
	
	@Qualifier
	@Retention(RUNTIME)
	@Target({ FIELD, TYPE, METHOD, PARAMETER })
	public @interface MockImplementation {
	}

}
