package com.hau.userservice.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.hau.commons.CommonQualifier.SelectedImplementation;
import com.hau.userservice.repositories.user.UserRepository;
import com.hau.userservice.repositories.user.entities.Role;
import com.hau.userservice.repositories.user.entities.User;
import com.hau.userservice.repositories.user.entities.UserRole;
import com.hau.userservice.repositories.user.entities.converters.UserConverter;
import com.hau.userservice.services.exceptions.InvalidInputException;
import com.hau.userservice.services.exceptions.NoContentException;
import com.hau.userservice.services.exceptions.NotFoundException;
import com.hau.userservice.services.exceptions.ServiceException;
import com.hau.userservice.services.impl.UserServiceImpl;
import com.hau.userservice.services.mocks.MockUserRepository;
import com.hau.userservice.services.models.UserData;
import com.hau.userservice.services.models.UserRoleData;
import com.hau.userservice.services.models.converters.UserDataConverter;

import junit.framework.Assert;

@RunWith(Arquillian.class)
public class UserServiceTest {

	List<UserRoleData> simpleRoleData = Arrays
			.asList(new UserRoleData(MockUserRepository.roleList.get("user").getName(), new Date().getTime()));

	List<UserRoleData> staffAndUserRolesData = Arrays.asList(
			new UserRoleData(MockUserRepository.roleList.get("user").getName(), new Date().getTime()),
			new UserRoleData(MockUserRepository.roleList.get("staff").getName(), new Date().getTime()));

	List<UserRoleData> inputInvalidRole = Arrays.asList(
			// valid role
			new UserRoleData(MockUserRepository.roleList.get("user").getName(), new Date().getTime()),
			// invalid role
			new UserRoleData("superadmin", new Date().getTime()));

	private boolean deepCompare(UserData user, UserData other) {
		if (user == null)
			return false;
		if (other == null)
			return false;
		if (user == other)
			return true;
		if (user.getExpiredDate() == null) {
			if (other.getExpiredDate() != null)
				return false;
		} else if (!user.getExpiredDate().equals(other.getExpiredDate()))
			return false;
		if (user.getId() == null) {
			if (other.getId() != null)
				return false;
		} else if (!user.getId().equals(other.getId()))
			return false;
		if (user.getIsActive() == null) {
			if (other.getIsActive() != null)
				return false;
		} else if (!user.getIsActive().equals(other.getIsActive()))
			return false;
		if (user.getPassword() == null) {
			if (other.getPassword() != null)
				return false;
		} else if (!user.getPassword().equals(other.getPassword()))
			return false;
		if (user.getRoles() == null) {
			if (other.getRoles() != null)
				return false;
		} else {
			if (user.getRoles().size() != other.getRoles().size())
				return false;
			for (UserRoleData userRole : user.getRoles()) {
				boolean found = false;
				for (UserRoleData otherRole : other.getRoles()) {
					if (userRole.equals(otherRole))
						found = true;
				}
				if (!found) 
					return false;
			}
		}
		if (user.getUsername() == null) {
			if (other.getUsername() != null)
				return false;
		} else if (!user.getUsername().equals(other.getUsername()))
			return false;

		return true;
	}
 
	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap.create(JavaArchive.class).addClass(UserDataConverter.class).addClass(UserConverter.class)
				.addClass(com.hau.commons.CommonQualifier.class)
				.addClasses(ServiceException.class, NotFoundException.class, InvalidInputException.class,
						NoContentException.class)
				.addClasses(UserData.class, UserRoleData.class).addClasses(User.class, Role.class, UserRole.class)
				.addClass(UserRepository.class).addClass(MockUserRepository.class).addClass(UserService.class)
				.addClass(UserServiceImpl.class).addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Inject
	@SelectedImplementation
	private UserService service;

	@Test(expected = NotFoundException.class)
	public void when_give_not_existed_user_id_to_get_method_then_it_throws_NotFoundException()
			throws NotFoundException {
		service.get(1);
	}

	@Test
	public void when_give_existed_user_id_to_get_method_then_it_returns_right_user_data() throws NotFoundException {
		UserData expectedUserData = UserDataConverter.fromEntity(MockUserRepository.mockUserList.get("testuser2"));
		UserData user = service.get(2);
		Assert.assertTrue(deepCompare(user, expectedUserData));
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_null_to_add_method_then_it_throws_InvalidInputException() throws InvalidInputException {
		service.add(null);
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_empty_data_to_add_method_then_it_throws_InvalidInputException() throws InvalidInputException {
		service.add(new UserData());
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_data_with_null_username_to_add_method_then_it_throws_InvalidInputException()
			throws InvalidInputException {
		service.add(new UserData((String) null, "123456", simpleRoleData, true, null));
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_data_with_username_less_than_2_chars_to_add_method_then_it_throws_InvalidInputException()
			throws InvalidInputException {
		service.add(new UserData("1", "123456", simpleRoleData, true, null));
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_data_with_username_more_than_20_chars_to_add_method_then_it_throws_InvalidInputException()
			throws InvalidInputException {
		service.add(new UserData("aaaaaaaaaaaaaaaaaaaaaa", "123456", simpleRoleData, true, null));
	}

	@Test
	public void when_give_data_with_username_with_3_chars_to_add_method_then_it_adds_succesfully()
			throws InvalidInputException {
		UserData mockUser = new UserData("abc", "123456", simpleRoleData, true, null);
		UserData result = service.add(mockUser);
		mockUser.setId(MockUserRepository.getCurrentId());
		mockUser.setPassword(null);
		Assert.assertTrue(deepCompare(result, mockUser));
	}

	@Test
	public void when_give_data_with_username_with_20_chars_to_add_method_then_it_adds_succesfully()
			throws InvalidInputException {
		UserData mockUser = new UserData("aaaaaaaaaaaaaaaaaaaa", "123456", simpleRoleData, true, null);
		UserData result = service.add(mockUser);
		mockUser.setId(MockUserRepository.getCurrentId());
		mockUser.setPassword(null);
		Assert.assertTrue(deepCompare(result, mockUser));
	}

	@Test
	public void when_give_data_with_username_with_16_chars_to_add_method_then_it_adds_succesfully()
			throws InvalidInputException {
		UserData mockUser = new UserData("aaaaaaaaaaaaaaaa", "123456", simpleRoleData, true, null);
		UserData result = service.add(mockUser);
		mockUser.setId(MockUserRepository.getCurrentId());
		mockUser.setPassword(null);
		Assert.assertTrue(deepCompare(result, mockUser));
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_data_with_an_invalid_role_to_add_method_then_it_throws_InvalidInputException()
			throws InvalidInputException {
		UserData mockUser = new UserData("invalidRole1", "123456", inputInvalidRole, true, null);
		service.add(mockUser);
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_data_with_null_roles_to_add_method_then_it_throws_InvalidInputException()
			throws InvalidInputException {
		UserData mockUser = new UserData("invalidRole1", "123456", null, true, null);
		service.add(mockUser);
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_data_with_empty_roles_to_add_method_then_it_throws_InvalidInputException()
			throws InvalidInputException {
		UserData mockUser = new UserData("invalidRole1", "123456", new ArrayList<>(), true, null);
		service.add(mockUser);
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_data_with_a_null_password_to_add_method_then_it_throws_InvalidInputException()
			throws InvalidInputException {
		UserData mockUser = new UserData("invalidPass1", null, simpleRoleData, true, null);
		service.add(mockUser);
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_data_with_an_empty_password_to_add_method_then_it_throws_InvalidInputException()
			throws InvalidInputException {

		UserData mockUser = new UserData("invalidPass2", "", simpleRoleData, true, null);
		service.add(mockUser);
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_data_with_a_password_less_than_4_chars_to_add_method_then_it_throws_InvalidInputException()
			throws InvalidInputException {
		UserData mockUser = new UserData("invalidPass3", "", simpleRoleData, true, null);
		service.add(mockUser);
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_data_with_username_has_existed_to_add_method_then_it_throws_InvalidInputException()
			throws InvalidInputException, NotFoundException {
		UserData mockUser = new UserData(service.get(2).getUsername(), "123456", simpleRoleData, true, null);
		service.add(mockUser);
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_data_with_expired_date_before_today_to_add_method_then_it_throws_InvalidInputException()
			throws InvalidInputException {
		UserData mockUser = new UserData("testExpiredDate1", "123456", simpleRoleData, true, 123L);
		service.add(mockUser);
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_data_with_expired_date_is_today_to_add_method_then_it_throws_InvalidInputException()
			throws InvalidInputException {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		UserData mockUser = new UserData("testExpiredDate2", "123456", simpleRoleData, true,
				calendar.getTime().getTime());
		service.add(mockUser);
	}

	@Test
	public void when_give_data_with_expired_date_is_tomorrow_then_it_adds_successfully() throws InvalidInputException {

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_YEAR, 1);
		long tomorrowTimestamp = calendar.getTime().getTime();

		UserData mockUser = new UserData("testExpiredDate4", "123456", simpleRoleData, true, tomorrowTimestamp);
		UserData result = service.add(mockUser);
		mockUser.setId(MockUserRepository.getCurrentId());
		mockUser.setPassword(null);
		Assert.assertTrue(deepCompare(result, mockUser));
	}

	@Test(expected = NotFoundException.class)
	public void when_give_a_user_id_that_not_exist_to_delete_method_then_it_NotFoundException()
			throws NotFoundException {
		service.delete(11111L);
	}

	@Test
	public void when_give_an_existed_user_id_to_delete_method_then_it_deletes_successfully()
			throws InvalidInputException, NotFoundException {
		UserData mockUser = new UserData("deleteTest1", "123456", simpleRoleData, true, null);
		UserData addResult = service.add(mockUser);
		mockUser.setId(MockUserRepository.getCurrentId());
		mockUser.setPassword(null);
		Assert.assertTrue(deepCompare(addResult, mockUser));

		UserData entity = service.get(mockUser.getId());
		Assert.assertTrue(deepCompare(entity, mockUser));

		UserData deleteResult = service.delete(mockUser.getId());
		Assert.assertTrue(deepCompare(deleteResult, mockUser));
	}

	@Test(expected = NotFoundException.class)
	public void when_give_a_user_id_that_not_exist_to_update_method_then_it_NotFoundException()
			throws NotFoundException, InvalidInputException {
		UserData mockUser = new UserData("updateTest1", "123456", simpleRoleData, true, null);
		service.update(11111, mockUser);
	}

	@Test
	public void when_give_an_existed_user_id_and_valid_update_data_to_update_method_then_it_updates_successfully()
			throws InvalidInputException, NotFoundException {
		UserData mockUser = new UserData("updateTest2", "123456", simpleRoleData, true, null);
		UserData addResult = service.add(mockUser);
		mockUser.setId(MockUserRepository.getCurrentId());
		mockUser.setPassword(null);
		Assert.assertTrue(deepCompare(addResult, mockUser));

		UserData entity = service.get(mockUser.getId());
		Assert.assertTrue(deepCompare(entity, mockUser));

		UserData updateData = new UserData();

		updateData.setPassword("12345678");

		updateData.setRoles(staffAndUserRolesData);
		mockUser.setRoles(staffAndUserRolesData);

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_YEAR, 10);
		updateData.setExpiredDate(calendar.getTime().getTime());
		mockUser.setExpiredDate(calendar.getTime().getTime());

		UserData updateResult = service.update(mockUser.getId(), updateData);
		Assert.assertTrue(deepCompare(updateResult, mockUser));
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_an_existed_user_id_and_update_username_data_to_update_method_then_it_it_throws_InvalidInputException()
			throws InvalidInputException, NotFoundException {
		UserData mockUser = new UserData("updateTest3", "123456", simpleRoleData, true, null);
		UserData addResult = service.add(mockUser);
		mockUser.setId(MockUserRepository.getCurrentId());
		mockUser.setPassword(null);
		Assert.assertTrue(deepCompare(addResult, mockUser));

		UserData entity = service.get(mockUser.getId());
		Assert.assertTrue(deepCompare(entity, mockUser));

		UserData updateData = new UserData();

		updateData.setPassword("12345678");

		updateData.setRoles(staffAndUserRolesData);
		mockUser.setRoles(staffAndUserRolesData);

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_YEAR, 10);
		updateData.setExpiredDate(calendar.getTime().getTime());
		mockUser.setExpiredDate(calendar.getTime().getTime());

		updateData.setUsername("updateTest3_fail_data");

		service.update(mockUser.getId(), updateData);
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_an_existed_user_id_and_update_invalid_password_to_update_method_then_it_it_throws_InvalidInputException()
			throws InvalidInputException, NotFoundException {
		UserData mockUser = new UserData("updateTest4", "123456", simpleRoleData, true, null);
		UserData addResult = service.add(mockUser);
		mockUser.setId(MockUserRepository.getCurrentId());
		mockUser.setPassword(null);
		Assert.assertTrue(deepCompare(addResult, mockUser));

		UserData entity = service.get(mockUser.getId());
		Assert.assertTrue(deepCompare(entity, mockUser));

		UserData updateData = new UserData();
		updateData.setPassword("123");

		service.update(mockUser.getId(), updateData);
		Assert.assertEquals(true, false);
	}

	@Test
	public void when_give_an_existed_user_id_and_update_valid_password_to_update_method_then_it_updates_successfully()
			throws InvalidInputException, NotFoundException {
		UserData mockUser = new UserData("updateTest5", "123456", simpleRoleData, true, null);
		UserData addResult = service.add(mockUser);
		mockUser.setId(MockUserRepository.getCurrentId());
		mockUser.setPassword(null);
		Assert.assertTrue(deepCompare(addResult, mockUser));

		UserData entity = service.get(mockUser.getId());
		Assert.assertTrue(deepCompare(entity, mockUser));

		UserData updateData = new UserData();
		updateData.setPassword("12345678");

		UserData updateResult = service.update(mockUser.getId(), updateData);
		Assert.assertTrue(deepCompare(updateResult, mockUser));
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_an_existed_user_id_and_update_invalid_roles_to_update_method_then_it_it_throws_InvalidInputException()
			throws InvalidInputException, NotFoundException {
		UserData mockUser = new UserData("updateTest6", "123456", simpleRoleData, true, null);
		UserData addResult = service.add(mockUser);
		mockUser.setId(MockUserRepository.getCurrentId());
		mockUser.setPassword(null);
		Assert.assertTrue(deepCompare(addResult, mockUser));

		UserData entity = service.get(mockUser.getId());
		Assert.assertTrue(deepCompare(entity, mockUser));

		UserData updateData = new UserData();
		updateData.setRoles(inputInvalidRole);

		service.update(mockUser.getId(), updateData);
	}

	@Test
	public void when_give_an_existed_user_id_and_update_valid_roles_to_update_method_then_it_updates_successfully() {
		try {
			UserData mockUser = new UserData("updateTest7", "123456", simpleRoleData, true, null);
			UserData addResult = service.add(mockUser);
			mockUser.setId(MockUserRepository.getCurrentId());
			mockUser.setPassword(null);
			Assert.assertTrue(deepCompare(addResult, mockUser));

			UserData entity = service.get(mockUser.getId());
			Assert.assertTrue(deepCompare(entity, mockUser));

			UserData updateData = new UserData();
			updateData.setRoles(staffAndUserRolesData);
			mockUser.setRoles(staffAndUserRolesData);

			UserData updateResult = service.update(mockUser.getId(), updateData);
			Assert.assertTrue(deepCompare(updateResult, mockUser));

		} catch (InvalidInputException e) {
			Assert.assertEquals(true, true);
		} catch (NotFoundException e) {
			Assert.assertEquals(true, false);
		} catch (Exception e) {
			Assert.assertEquals(true, false);
		}
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_an_existed_user_id_and_update_invalid_expried_date_to_update_method_then_it_it_throws_InvalidInputException()
			throws InvalidInputException, NotFoundException {

		UserData mockUser = new UserData("updateTest8", "123456", simpleRoleData, true, null);
		UserData addResult = service.add(mockUser);
		mockUser.setId(MockUserRepository.getCurrentId());
		mockUser.setPassword(null);
		Assert.assertTrue(deepCompare(addResult, mockUser));

		UserData entity = service.get(mockUser.getId());
		Assert.assertTrue(deepCompare(entity, mockUser));

		UserData updateData = new UserData();
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		updateData.setExpiredDate(calendar.getTime().getTime());

		service.update(mockUser.getId(), updateData);
	}

	@Test
	public void when_give_an_existed_user_id_and_update_valid_expired_date_to_update_method_then_it_updates_successfully()
			throws InvalidInputException, NotFoundException {
		UserData mockUser = new UserData("updateTest9", "123456", simpleRoleData, true, null);
		UserData addResult = service.add(mockUser);
		mockUser.setId(MockUserRepository.getCurrentId());
		mockUser.setPassword(null);
		Assert.assertTrue(deepCompare(addResult, mockUser));

		UserData entity = service.get(mockUser.getId());
		Assert.assertTrue(deepCompare(entity, mockUser));

		UserData updateData = new UserData();
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_YEAR, 10);
		updateData.setExpiredDate(calendar.getTime().getTime());
		mockUser.setExpiredDate(calendar.getTime().getTime());

		UserData updateResult = service.update(mockUser.getId(), updateData);
		Assert.assertTrue(deepCompare(updateResult, mockUser));
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_invalid_page_to_list_method_then_it_throws_InvalidInputException()
			throws NoContentException, InvalidInputException {
		service.list(0, 5);
	}

	@Test(expected = InvalidInputException.class)
	public void when_give_invalid_page_size_to_list_method_then_it_throws_InvalidInputException()
			throws NoContentException, InvalidInputException {
		service.list(1, 4);
	}
	
	@Test
	public void when_give_valid_params_to_list_method_then_it_returns_a_none_empty_list()
			throws NoContentException, InvalidInputException {
		Assert.assertFalse(service.list(1, 5).isEmpty()); 
	} 
	
	@Test(expected = NoContentException.class)
	public void when_give_valid_params_to_list_method_then_it_returns_an_empty_list()
			throws NoContentException, InvalidInputException, NotFoundException {
		Collection<UserData> check = service.list(1, 1000);
		
		// delete all
		for (UserData user : check) {
			service.delete(user.getId());
		}
		
		try {
			service.list(1, 5);
		} finally {
			Collection<User> mockUsers = MockUserRepository.mockUserList.values();
			for (User user : mockUsers) {
				UserData data = UserDataConverter.fromEntity(user);
				data.setPassword(user.getPassword());
				service.add(data);
			}
		}
	}
	
	
	@Test
	public void when_give_mock_param_value_to_findByUsername_method_then_it_returns_a_none_empty_list()
			throws NoContentException, InvalidInputException {
		Assert.assertFalse(service.findByUsername(MockUserRepository.MOCK_NONE_EMPTY_SEARCH_PARAM).isEmpty()); 
	}
	
	@Test(expected = NoContentException.class)
	public void when_give_none_mock_param_value_to_findByUsername_method_then_it_returns_a_none_empty_list()
			throws NoContentException, InvalidInputException {
		service.findByUsername("NOT"+MockUserRepository.MOCK_NONE_EMPTY_SEARCH_PARAM);
	}
	

}
