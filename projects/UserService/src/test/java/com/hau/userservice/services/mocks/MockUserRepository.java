package com.hau.userservice.services.mocks;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;

import com.google.common.collect.Maps;
import com.hau.commons.CommonQualifier.MockImplementation;
import com.hau.commons.CommonQualifier.SelectedImplementation;
import com.hau.userservice.repositories.user.UserRepository;
import com.hau.userservice.repositories.user.entities.Role;
import com.hau.userservice.repositories.user.entities.User;
import com.hau.userservice.repositories.user.entities.UserRole;
import com.hau.userservice.repositories.user.entities.converters.UserConverter;
import com.hau.userservice.services.models.UserData;
import com.hau.userservice.services.models.UserRoleData;

@MockImplementation
@SelectedImplementation
public class MockUserRepository implements UserRepository {
	public static final Map<String, Role> roleList;
	
	public static final Map<String, User> mockUserList;
	
	public static final String MOCK_NONE_EMPTY_SEARCH_PARAM = "none_empty";

	
	private static long currentId;	
	
	public static long getCurrentId() {
		return currentId;
	}
	
	static {
		roleList = new HashMap<>();
		Role user = new Role("user", "System User.");
		user.setId(1);
		roleList.put("user", user);
		Role staff = new Role("staff", "System Staff.");
		staff.setId(2);
		roleList.put("staff", staff);
		Role admin = new Role("admin", "System admin.");
		admin.setId(3);
		roleList.put("admin", admin);
		
		
		mockUserList = new HashMap<>();
		User userId2 = new User("testuser2", "123456", true, null);
		userId2.setId(2);
		List<UserRole> userId2Roles = new ArrayList<>();
		userId2Roles.add(new UserRole(userId2, user, new Date()));
		userId2.setUserRoles(userId2Roles);
		mockUserList.put("testuser2", userId2);
		
		User userId3 = new User("testuser3", "123456", true, null);
		userId3.setId(3);
		List<UserRole> userId3Roles = new ArrayList<>();
		userId3Roles.add(new UserRole(userId3, user, new Date()));
		userId3.setUserRoles(userId3Roles);
		mockUserList.put("testuser3", userId3);
		
		User userId4 = new User("testuser4", "123456", true, null);
		userId4.setId(4);
		List<UserRole> userId4Roles = new ArrayList<>();
		userId4Roles.add(new UserRole(userId4, user, new Date()));
		userId4.setUserRoles(userId3Roles);
		mockUserList.put("testuser4", userId4);
		
		User userId5 = new User("testuser5", "123456", true, null);
		userId5.setId(5);
		List<UserRole> userId5Roles = new ArrayList<>();
		userId5Roles.add(new UserRole(userId5, user, new Date()));
		userId5.setUserRoles(userId5Roles);
		mockUserList.put("testuser5", userId5);
		
		User userId6 = new User("testuser6", "123456", true, null);
		userId6.setId(6);
		List<UserRole> userId6Roles = new ArrayList<>();
		userId6Roles.add(new UserRole(userId6, user, new Date()));
		userId6.setUserRoles(userId6Roles);
		mockUserList.put("testuser6", userId6);
		
		currentId = 6;
	}
	
	private final Map<String, Role> dataRoleList;
	
	private final Map<String, User> dataUserList;
	

	public MockUserRepository() {
		this.dataRoleList = new HashMap<>();
		
		roleList.keySet().forEach(role -> dataRoleList.put(role, roleList.get(role)));
		
		this.dataUserList = new HashMap<>();
		
		mockUserList.keySet().forEach(userKey -> dataUserList.put(userKey, mockUserList.get(userKey)));
	}

	@Override
	public User get(long userId) {
		Collection<User> set = this.dataUserList.values();
		for (User user : set) {
			if (user.getId() == userId)
				return user;
		}
		return null;
	}

	@Override
	public Collection<User> findUsers(String searchValue) {
		if (searchValue.equals(MOCK_NONE_EMPTY_SEARCH_PARAM))
			return this.dataUserList.values();
		
		return null;
	}

	@Override
	public User add(UserData userApiModel) {
		User entity = UserConverter.fromApiModel(userApiModel);
		
		List<UserRole> newUserRoles = new ArrayList<>();
		

		Collection<Role> roles = this.dataRoleList.values();
		for (UserRoleData roleData : userApiModel.getRoles()) {
			for (Role role : roles) {
				if (role.getName().equals(roleData.getRole())) {
					newUserRoles.add(new UserRole(entity, role, new Date()));
				}
			}
		}
		
		entity.setUserRoles(newUserRoles);
		
		
		entity.setId(++currentId);
		this.dataUserList.put(entity.getUsername(), entity);
		
		
		return entity;
	}

	@Override
	public User update(long userId, UserData userApiModel) {
		Collection<User> userSet = this.dataUserList.values();
		for (User user : userSet) {
			if (user.getId()==userId) {
				if (userApiModel.getPassword()!=null)
					user.setPassword(userApiModel.getPassword());
				if (userApiModel.getExpiredDate()!=null)
					user.setExpiredDate(new Date(userApiModel.getExpiredDate()));
				if (userApiModel.getRoles()!=null) {
					Collection<Role> roles = this.dataRoleList.values();
					List<UserRole> newUserRoles = new ArrayList<>();
					for (UserRoleData roleData : userApiModel.getRoles()) {
						for (Role role : roles) {
							if (role.getName().equals(roleData.getRole())) {
								newUserRoles.add(new UserRole(user, role, new Date()));
							}
						}
					}
					user.setUserRoles(newUserRoles);
				}
				return user;
			}
		}
		
		return null;
	}

	@Override
	public User find(String username) {
		Collection<User> userSet = this.dataUserList.values();
		for (User user : userSet) {
			if (user.getUsername().equals(username)) {
				return user;
			}
		}
		
		return null;
	}

	@Override
	public User delete(long userId) {
		Collection<User> userSet = this.dataUserList.values();
		for (User user : userSet) {
			if (user.getId()==userId) {
				this.dataUserList.remove(user.getUsername());
				return user;
			}
		}
		
		return null;
	}

	@Override
	public Collection<User> list(int page, int pageSize) {
		ArrayList<User> rs = new ArrayList<>();
		rs.addAll(this.dataUserList.values());
		
		int offset = pageSize * (page - 1);
		
		int max = offset+pageSize - 1;
		if (max>=rs.size())
			max = rs.size()-1;
		
		return rs.subList(offset, max);
	}

	@Override
	public Collection<Role> listSystemRoles() {
		return this.dataRoleList.values();
	}

}
