USE [master]
GO
/****** Object:  Database [goldenExample]    Script Date: 12/25/2017 3:26:43 PM ******/
CREATE DATABASE [goldenExample]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'goldenExample', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\goldenExample.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'goldenExample_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\goldenExample_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [goldenExample] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [goldenExample].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [goldenExample] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [goldenExample] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [goldenExample] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [goldenExample] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [goldenExample] SET ARITHABORT OFF 
GO
ALTER DATABASE [goldenExample] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [goldenExample] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [goldenExample] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [goldenExample] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [goldenExample] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [goldenExample] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [goldenExample] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [goldenExample] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [goldenExample] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [goldenExample] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [goldenExample] SET  DISABLE_BROKER 
GO
ALTER DATABASE [goldenExample] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [goldenExample] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [goldenExample] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [goldenExample] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [goldenExample] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [goldenExample] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [goldenExample] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [goldenExample] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [goldenExample] SET  MULTI_USER 
GO
ALTER DATABASE [goldenExample] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [goldenExample] SET DB_CHAINING OFF 
GO
ALTER DATABASE [goldenExample] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [goldenExample] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [goldenExample]
GO
/****** Object:  Table [dbo].[role]    Script Date: 12/25/2017 3:26:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[role](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](20) NOT NULL,
	[description] [varchar](50) NULL,
 CONSTRAINT [PK_role] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user]    Script Date: 12/25/2017 3:26:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[username] [varchar](50) NOT NULL,
	[password] [varchar](128) NOT NULL,
	[is_active] [bit] NOT NULL,
	[expired_date] [datetime] NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user_role]    Script Date: 12/25/2017 3:26:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_role](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[user_id] [bigint] NOT NULL,
	[role_id] [int] NOT NULL,
	[issued_date] [datetime] NOT NULL,
 CONSTRAINT [PK_user_role] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[role] ON 

INSERT [dbo].[role] ([id], [name], [description]) VALUES (1, N'user', N'System user.')
INSERT [dbo].[role] ([id], [name], [description]) VALUES (2, N'staff', N'System Staff.')
INSERT [dbo].[role] ([id], [name], [description]) VALUES (3, N'admin', N'Administrator.')
SET IDENTITY_INSERT [dbo].[role] OFF
SET IDENTITY_INSERT [dbo].[user] ON 

INSERT [dbo].[user] ([id], [username], [password], [is_active], [expired_date]) VALUES (1, N'cachua', N'123456', 1, NULL)
INSERT [dbo].[user] ([id], [username], [password], [is_active], [expired_date]) VALUES (2, N'catim', N'123456', 0, NULL)
INSERT [dbo].[user] ([id], [username], [password], [is_active], [expired_date]) VALUES (4, N'caphao', N'123456', 1, NULL)
INSERT [dbo].[user] ([id], [username], [password], [is_active], [expired_date]) VALUES (5, N'vanteo', N'123456', 1, NULL)
INSERT [dbo].[user] ([id], [username], [password], [is_active], [expired_date]) VALUES (6, N'vangnoc', N'123456', 1, CAST(0x0000A85800000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[user] OFF
SET IDENTITY_INSERT [dbo].[user_role] ON 

INSERT [dbo].[user_role] ([id], [user_id], [role_id], [issued_date]) VALUES (1, 1, 1, CAST(0x0000A85400000000 AS DateTime))
INSERT [dbo].[user_role] ([id], [user_id], [role_id], [issued_date]) VALUES (2, 1, 3, CAST(0x0000A85400000000 AS DateTime))
INSERT [dbo].[user_role] ([id], [user_id], [role_id], [issued_date]) VALUES (3, 2, 1, CAST(0x0000A85400000000 AS DateTime))
INSERT [dbo].[user_role] ([id], [user_id], [role_id], [issued_date]) VALUES (9, 4, 1, CAST(0x0000A85400000000 AS DateTime))
INSERT [dbo].[user_role] ([id], [user_id], [role_id], [issued_date]) VALUES (10, 4, 2, CAST(0x0000A85400000000 AS DateTime))
INSERT [dbo].[user_role] ([id], [user_id], [role_id], [issued_date]) VALUES (11, 5, 1, CAST(0x0000A85400000000 AS DateTime))
INSERT [dbo].[user_role] ([id], [user_id], [role_id], [issued_date]) VALUES (12, 5, 1, CAST(0x0000A85400000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[user_role] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_user]    Script Date: 12/25/2017 3:26:43 PM ******/
ALTER TABLE [dbo].[user] ADD  CONSTRAINT [IX_user] UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[user_role]  WITH CHECK ADD  CONSTRAINT [FK_user_role_role] FOREIGN KEY([role_id])
REFERENCES [dbo].[role] ([id])
GO
ALTER TABLE [dbo].[user_role] CHECK CONSTRAINT [FK_user_role_role]
GO
ALTER TABLE [dbo].[user_role]  WITH CHECK ADD  CONSTRAINT [FK_user_role_user] FOREIGN KEY([user_id])
REFERENCES [dbo].[user] ([id])
GO
ALTER TABLE [dbo].[user_role] CHECK CONSTRAINT [FK_user_role_user]
GO
USE [master]
GO
ALTER DATABASE [goldenExample] SET  READ_WRITE 
GO
